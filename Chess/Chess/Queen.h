#pragma once
#include <iostream>
#include <string>
#include "Position.h"
#include "Board.h"

class Queen : public Piece
{
public:

	Queen(Position source, char type, bool isWhite);
	~Queen();
	bool isLegalMove(Position dest, Piece* chessBoard[8][8]);
	bool isBlocked(Position dest, Piece* chessBoard[8][8]);

private:



};
#pragma once
#include <iostream>
#include <string>
#include "Position.h"
#include "Board.h"

class Board
{
public:

	std::string receive();
	void sendBoard();
	void send(std::string str);

private:

	std::string msg;

};

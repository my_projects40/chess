#include <iostream>
#include <string>

#include "Position.h"
#include "Board.h"
#include "Piece.h"

#include "King.h"
#include "Rook.h"
#include "Pawn.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"

Board::Board()
{
	int i = 0;
	int j = 0;
	_whiteKing = { 0 ,4 };
	_blackKing = { 7, 4 };
	_player = 0;

	Position temp = Position( 0, 4 );

	for (i = 0; i < 8; i++)
	{
		for (j = 0; j < 8; j++)
		{
			this->pieces[i][j] = nullptr;
		}
	}
	pieces[0][4] = new King(temp, 'K', true);
	temp.setX(4);
	temp.setY(7);
	pieces[7][4] = new King(temp, 'k', false);


	for (i = 0; i < 8; i++) {
		temp.setX(i);
		temp.setY(1);
		pieces[1][i] = new Pawn(temp, 'P', true);
	}

	for (i = 0; i < 8; i++) {
		temp.setX(i);
		temp.setY(6);
		pieces[6][i] = new Pawn(temp, 'p', true);
	}


	temp.setX(0);
	temp.setY(0);
	pieces[0][0] = new Rook(temp, 'R', true);

	temp.setX(7);
	temp.setY(0);
	pieces[0][7] = new Rook(temp, 'R', true);

	temp.setX(0);
	temp.setY(7);
	pieces[7][0] = new Rook(temp, 'r', false);

	temp.setX(7);
	temp.setY(7);
	pieces[7][7] = new Rook(temp, 'r', false);


	temp.setX(1);
	temp.setY(0);
	pieces[0][1] = new Knight(temp, 'N', true);

	temp.setX(6);
	temp.setY(0);
	pieces[0][6] = new Knight(temp, 'N', true);

	temp.setX(1);
	temp.setY(7);
	pieces[7][1] = new Knight(temp, 'n', false);

	temp.setX(6);
	temp.setY(7);
	pieces[7][6] = new Knight(temp, 'n', false);

	temp.setX(2);
	temp.setY(0);
	pieces[0][2] = new Bishop(temp, 'B', true);

	temp.setX(5);
	temp.setY(0);
	pieces[0][5] = new Bishop(temp, 'B', true);
	
	temp.setX(2);
	temp.setY(7);
	pieces[7][2] = new Bishop(temp, 'b', false);

	temp.setX(5);
	temp.setY(7);
	pieces[7][5] = new Bishop(temp, 'b', false);
	
	temp.setX(3);
	temp.setY(0);
	pieces[0][3] = new Queen(temp, 'Q', true);

	temp.setX(3);
	temp.setY(7);
	pieces[7][3] = new Queen(temp, 'q', false);
	_player = 0; //white
}
Board::~Board()
{

}
int Board::getPlayer() { return this->_player; }

void Board::switchPlayer()
{
	if (_player == 1)
	{
		_player = 0;
	}
	else
	{
		_player = 1;
	}
}

void Board::printBoard() {
	std::cout << "\n";
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if (this->pieces[i][j] != nullptr)
			{
				std::cout << this->pieces[i][j]->getId();
			}
			else
			{
				std::cout << "#";
			}
			
		}
		std::cout << "\n";
	}
}

Position Board::translator(std::string str)
{
	char c[3];
	strcpy_s(c, str.c_str());
	if (isupper(c[0])) {
		c[0] = tolower(c[0]);
	}
	int num = int(c[0]);
	num = num - 97;
	
	//std::cout << c[1] - '1' << "," << num << "\n";
	return Position(c[1] - '1', num); //why
}
int Board::checkForCheck()
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if (this->pieces[i][j] != nullptr)
			{
				if (this->pieces[i][j]->getisWhite()) {
					if (this->pieces[i][j]->isLegalMove(_blackKing, pieces))
					{
						if (this->pieces[i][j]->isBlocked(_blackKing, pieces) == false) {
							return 2;
						}
					}
				}
				else{
					if (this->pieces[i][j]->isLegalMove(_whiteKing, pieces))
					{
						if (this->pieces[i][j]->isBlocked(_whiteKing, pieces) == false) {
							return 1;
						}
					}
				}
			}

		}
	}
	return 0;
}
//3
std::string Board::movePiece(std::string str)
{
	bool flag = true;
	std::string error_str = "";
	Position source = translator(str.substr(0, 2));
	Position dest = translator(str.substr(2, 4));

	if (pieces[source.getY()][source.getX()]->isBlocked(dest, this->pieces))
	{
		error_str = "6";
		std::cout << 61;
		return error_str; //error
	}
	if (dest.getX() > 7 || dest.getX() < 0) {
		error_str = "6";

		std::cout << 62;
		return error_str; //error
	}
	if (dest.getY() > 7 || dest.getY() < 0) {
		error_str = "6";
		std::cout << 63;
		return error_str; //error
	}
	if (source.getX() == dest.getX() && source.getY() == dest.getY())
	{
		error_str = "7";
		std::cout << 71;
		return error_str; //error
	}
	if ((source.getX() < 0 || source.getX() > 7) || (dest.getX() < 0 || dest.getX() > 7) || (source.getY() < 0 || source.getY() > 7) ||  ( dest.getY() < 0 || dest.getY() > 7))
	{	
		error_str = "5";
		std::cout << 51;
		return error_str; //error
	}
	if (pieces[source.getY()][source.getX()] == nullptr)
	{
		error_str = "2";
		std::cout << 21;
		return error_str; //error
	}

	/*if (pieces[dest.getY()][dest.getX()]->getisWhite() == true && _player == 0) {
		error_str = "2";
		std::cout << 21;
		return error_str; //error
	}*/
	if (pieces[source.getY()][source.getX()]->getisWhite() && _player == 1 || !pieces[source.getY()][source.getX()]->getisWhite() && _player == 0)//1 - black
	{
		error_str = "2";
		//error_str[1] = '\0';
		//std::cout << error_str;
		std::cout << 22;
		return error_str; //error
	}

/*
	if (pieces[dest.getY()][dest.getX()]->getisWhite() == true && _player == 0 || pieces[dest.getY()][dest.getX()]->getisWhite() == false && _player == 1)
	{
		error_str = "2";
		//std::cout << error_str;
		return error_str; //error
		std::cout << 23;
	}
*/
	flag = pieces[source.getY()][source.getX()]->isLegalMove(dest, pieces);
	if (flag == true)
	{
		error_str = "0";
		if (dest.getX() == _whiteKing.getX() && dest.getY() == _whiteKing.getY())
		{
			error_str = "8";
		}
		if (dest.getX() == _blackKing.getX() && dest.getY() == _blackKing.getY())
		{
			error_str = "8";
		}
		if (this->checkForCheck() != 0) {
			return "1";
		}
		std::cout << "--------\n";
		changePosition(source, dest);
		switchPlayer();
		return error_str; //error
	}
	else
	{
		std::cout << "\nIlegal move\n";
		error_str = "6";
		//error_str[1] = '\0';
		//std::cout << error_str;
		return error_str; //error
	}
}

void Board::changePosition(Position source, Position dest)
{
	if (this->pieces[source.getY()][source.getX()]->getId() == 'K') {
		this->_whiteKing = dest;
	}
	if (this->pieces[source.getY()][source.getX()]->getId() == 'k') {
		this->_blackKing = dest;
	}
	this->pieces[source.getY()][source.getX()]->setPos(dest);
	this->pieces[dest.getY()][dest.getX()] = this->pieces[source.getY()][source.getX()];
	this->pieces[source.getY()][source.getX()] = nullptr;
}

//getters& setters
Piece** Board::getPieces()
{
	return *pieces;
}



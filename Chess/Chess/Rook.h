#pragma once
#include <iostream>
#include <string>
#include "Position.h"
#include "Board.h"

class Rook : public Piece
{
public:

	Rook(Position source, char type, bool isWhite);
	~Rook();
	bool isLegalMove(Position dest, Piece* chessBoard[8][8]) override;
	bool isBlocked(Position dest, Piece* chessBoard[8][8]) override;

private:

};
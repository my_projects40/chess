#include <iostream>
#include <string>
#include "Piece.h"
#include "Board.h"
#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"

Queen::Queen(Position source, char type, bool isWhite) : Piece(source, type, isWhite)
{

}
Queen::~Queen()
{

}

bool Queen::isLegalMove(Position dest, Piece* chessBoard[8][8])//bishop rook
{
	int posY = this->getPos().getY();
	int posX = this->getPos().getX();
	int yDist = posY - dest.getY();
	int xDist = posX - dest.getX();

	if (yDist < 0) {
		yDist = yDist * -1;
	}

	if (xDist < 0) {
		xDist = xDist * -1;
	}

	if (yDist == xDist) {

		return true;
	}

	if ((this->getPos().getX() == dest.getX() && (this->getPos().getY() < dest.getY() || this->getPos().getY() > dest.getY())) ||
		(this->getPos().getY() == dest.getY() && (this->getPos().getX() < dest.getX() || this->getPos().getX() > dest.getX())))
	{
		return true;
	}

	return false;
}
bool Queen::isBlocked(Position dest, Piece* chessBoard[8][8])
{
	
	int posY = this->getPos().getY();
	int posX = this->getPos().getX();
	int i = 0;
	int j = 0;
	if (posX < dest.getX())
	{
		for (i = this->getPos().getX() + 1; i < dest.getX(); i++) {
			if (chessBoard[posY][i] != nullptr) {
				return true;
			}
		}
	}
	//left
	if (posX > dest.getX())
	{
		for (i = this->getPos().getX() + 1; i > dest.getX(); i--) {
			if (chessBoard[posY][i] != nullptr) {
				return true;
			}
		}
	}
	//down
	if (posY < dest.getY())
	{
		for (i = this->getPos().getY() + 1; i < dest.getY(); i++) {
			if (chessBoard[i][posX] != nullptr) {
				std::cout << chessBoard[i][posX]->getId();
				return true;
			}
		}
	}
	//up
	if (posY > dest.getY())
	{
		for (i = this->getPos().getY() + 1; i > dest.getY(); i--) {
			if (chessBoard[i][posX] != nullptr) {
				return true;
			}
		}
	}

	/*
	if (posX < dest.getX())
	{
		if (posY < dest.getY())
		{
			for (i = this->getPos().getX(); i < dest.getX(); i++) {
				for (j = this->getPos().getY(); j < dest.getY(); j++)
				{
					if (chessBoard[j + 1][i + 1] != nullptr) {
						return true;
					}
				}
			}
		}
		else if (posY > dest.getY())
		{
			for (i = this->getPos().getX(); i < dest.getX(); i++) {
				for (j = this->getPos().getY(); j > dest.getY(); j--)
				{
					if (chessBoard[j - 1][i + 1] != nullptr) {
						return true;
					}
				}

			}
		}
	}
	else if (posX > dest.getX())
	{
		if (posY < dest.getY())
		{
			for (i = this->getPos().getX(); i > dest.getX(); i--) {
				for (j = this->getPos().getY(); j < dest.getY(); j++)
				{
					if (chessBoard[j + 1][i - 1] != nullptr) {
						return true;
					}
				}
			}
		}
		else if (posY > dest.getY())
		{
			for (i = this->getPos().getX(); i > dest.getX(); i--) {
				for (j = this->getPos().getY(); j > dest.getY(); j--)
				{
					if (chessBoard[j - 1][i - 1] != nullptr) {
						return true;
					}
				}

			}
		}
	}
	*/
	return false;
}
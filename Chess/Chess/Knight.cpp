#include <iostream>
#include <string>
#include "Piece.h"
#include "Board.h"
#include "Knight.h"

Knight::Knight(Position source, char type, bool isWhite) : Piece(source, type, isWhite)
{

}

Knight::~Knight() 
{

}


bool Knight::isLegalMove(Position dest, Piece* chessBoard[8][8])
{
	if (this->getPos().getY() == dest.getY() - 2)
	{
		if (this->getPos().getX() == dest.getX() + 1 || this->getPos().getX() == dest.getX() - 1)
		{
			return true;
		}
	}

	if (this->getPos().getY() == dest.getY() + 2)
	{
		if (this->getPos().getX() == dest.getX() + 1 || this->getPos().getX() == dest.getX() - 1)
		{
			return true;
		}
	}

	if (this->getPos().getX() == dest.getX() - 2)
	{
		if (this->getPos().getY() == dest.getY() + 1 || this->getPos().getY() == dest.getY() - 1)
		{
			return true;
		}
	}

	if (this->getPos().getX() == dest.getX() + 2)
	{

		if (this->getPos().getY() == dest.getY() + 1 || this->getPos().getY() == dest.getY() - 1)
		{
			return true;
		}
	}
	return false;
}
bool Knight::isBlocked(Position dest, Piece* chessBoard[8][8])
{
	return false;
}
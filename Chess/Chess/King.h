#pragma once
#include <iostream>
#include <string>
#include "Position.h"
#include "Board.h"

class King : public Piece
{
public:
	King(Position source, char type, bool isWhite);
	~King();
	bool isLegalMove(Position dest, Piece* chessBoard[8][8]) override;
	bool isBlocked(Position dest, Piece* chessBoard[8][8]) override ;

private:


};
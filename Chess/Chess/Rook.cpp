#include <iostream>
#include <string>
#include "Piece.h"
#include "Board.h"
#include "Rook.h"



Rook::Rook(Position source, char type, bool isWhite) : Piece(source, type, isWhite)
{

}
Rook::~Rook()
{

}

bool Rook::isLegalMove(Position dest, Piece* chessBoard[8][8])
{
	if ((this->getPos().getX() == dest.getX() && (this->getPos().getY() < dest.getY() || this->getPos().getY() > dest.getY()))||   
		(this->getPos().getY() == dest.getY() && (this->getPos().getX() < dest.getX() || this->getPos().getX() > dest.getX())))
	{
		return true;
	}
	return false;
}
bool Rook::isBlocked(Position dest, Piece* chessBoard[8][8])
{
	int posY = this->getPos().getY();
	int posX = this->getPos().getX();
	int i = 0;

	//right
	if (posX < dest.getX())
	{
		for (i = this->getPos().getX() + 1; i < dest.getX(); i++) {
			if (chessBoard[posY][i] != nullptr) {
				return true;
			}
		}
	}
	//left
	if (posX > dest.getX())
	{
		for (i = this->getPos().getX() + 1; i > dest.getX(); i--) {
			if (chessBoard[posY][i] != nullptr) {
				return true;
			}
		}
	}
	//down
	if (posY < dest.getY())
	{
		for (i = this->getPos().getY() + 1; i < dest.getY(); i++) {
			if (chessBoard[i][posX] != nullptr) {
				std::cout << chessBoard[i][posX]->getId();
				return true;
			}
		}
	}
	//up
	if (posY > dest.getY())
	{
		for (i = this->getPos().getY() + 1; i > dest.getY(); i--) {
			if (chessBoard[i][posX] != nullptr) {
				return true;
			}
		}
	}

	return false;
}
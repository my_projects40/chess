#include "Position.h"

Position::Position(int y, int x) : _y(y), _x(x) {}

Position::Position() : _x(1), _y(1) {}

int Position::getX()
{
	return this->_x;
}
int Position::getY()
{
	return this->_y;
}
void Position::setX(int x)
{
	this->_x = x;
}
void Position::setY(int y)
{
	this->_y = y;
}

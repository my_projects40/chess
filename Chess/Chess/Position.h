#pragma once
#include <iostream>

class Position 
{
public:
	Position(int y, int x);
	Position();
	int getX();
	int getY();
	void setX(int x);
	void setY(int y);

private:

	int _x, _y;

};
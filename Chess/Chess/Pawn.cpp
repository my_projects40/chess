#include <iostream>
#include <string>
#include "Position.h"
#include "Piece.h"
#include "Board.h"
#include "Pawn.h"

Pawn::Pawn(Position source, char type, bool isWhite) : Piece(source, type, isWhite)
{

}
Pawn::~Pawn()
{

}

bool Pawn::isLegalMove(Position dest, Piece* chessBoard[8][8])
{
	if (this->getisWhite())//white
	{
		if (this->getPos().getY() == dest.getY() - 1)//eating, white
		{
			if (this->getPos().getX() == dest.getX() + 1 || this->getPos().getX() == dest.getX() - 1)
			{
				if (chessBoard[dest.getY()][dest.getX()] != nullptr) {
					return true;
				}
			}
		}


		if (this->getPos().getY() == 1)//move 2 or 1
		{
			if ((this->getPos().getX() == dest.getX()))
			{
				if (this->getPos().getY() + 1 == dest.getY() || this->getPos().getY() + 2 == dest.getY())
				{
					if (chessBoard[dest.getY()][dest.getX()] == nullptr) {
						return true;
					}
				}
			}
		}
		else //move 1
		{
			if ((this->getPos().getX() == dest.getX()) && (this->getPos().getY() + 1 == dest.getY()))
			{
				if (chessBoard[dest.getY()][dest.getX()] == nullptr) {
					return true;
				}
			}
		}
	}
	else
	{
		if (this->getPos().getY() == dest.getY() + 1)
		{
			if (this->getPos().getX() == dest.getX() + 1 || this->getPos().getX() == dest.getX() - 1)
			{
				if (chessBoard[dest.getY()][dest.getX()] != nullptr) {
					return true;
				}
			}
		}

		if (this->getPos().getY() == 6)
		{
			if ((this->getPos().getX() == dest.getX()))
			{
				if (this->getPos().getY() - 1 == dest.getY() || this->getPos().getY() - 2 == dest.getY())
				{
					if (chessBoard[dest.getY()][dest.getX()] == nullptr) {
						return true;
					}
				}
			}
		}
		else
		{
			if ((this->getPos().getX() == dest.getX()) && (this->getPos().getY() - 1 == dest.getY()))
			{
				if (chessBoard[dest.getY()][dest.getX()] == nullptr) {
					return true;
				}
			}
		}
	}

	return false;
}

bool Pawn::isBlocked(Position dest, Piece* chessBoard[8][8])
{
	int posY = this->getPos().getY();
	int posX = this->getPos().getX();
	int i = 0;

	if (posY < dest.getY())
	{
		if (chessBoard[this->getPos().getY()][posX] == nullptr) {
			return true;
		}
	}
	if (posY > dest.getY())
	{
		if (chessBoard[this->getPos().getY()][posX] == nullptr) {
			return true;
		}
	}

	return false;
}

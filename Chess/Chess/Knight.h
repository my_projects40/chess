#pragma once
#include <iostream>
#include <string>
#include "Position.h"
#include "Board.h"

class Knight : public Piece
{
public:
	Knight(Position source, char type, bool isWhite);
	~Knight();
	bool isLegalMove(Position dest, Piece* chessBoard[8][8]);
	bool isBlocked(Position dest, Piece* chessBoard[8][8]);


private:

};
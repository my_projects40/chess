#pragma once
#include <iostream>
#include <string>
#include "Position.h"
#include "Board.h"

class Pawn : public Piece
{
public:

	Pawn(Position source, char type, bool isWhite);
	~Pawn();
	bool isLegalMove(Position dest, Piece* chessBoard[8][8]);
	bool isBlocked(Position dest, Piece* chessBoard[8][8]);

private:

};
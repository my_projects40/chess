#pragma once
#include <iostream>
#include <string>
#include "Position.h"
#include "Board.h"
#include "Piece.h"

class Bishop : public Piece
{

public:
	Bishop(Position source, char type, bool isWhite);
	~Bishop();
	bool isLegalMove(Position dest, Piece* chessBoard[8][8]);
	bool isBlocked(Position dest, Piece* chessBoard[8][8]);

private:

};
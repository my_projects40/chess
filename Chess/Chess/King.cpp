#include <iostream>
#include <string>
#include "Piece.h"
#include "Board.h"
#include "King.h"

King::King(Position source, char type, bool isWhite) : Piece(source, type, isWhite)
{

}
King::~King()
{

}
//true = 0
//false = 6
bool King::isLegalMove(Position dest, Piece* chessBoard[8][8])
{
	if (this->getPos().getX() == dest.getX() + 1 || this->getPos().getX() == dest.getX() - 1 || this->getPos().getX() == dest.getX())
	{
		if (this->getPos().getY() == dest.getY() + 1 || this->getPos().getY() == dest.getY() - 1 || this->getPos().getY() == dest.getY())
		{

			return true;
		}
	}
	

	return false;
}

bool King::isBlocked(Position dest, Piece* chessBoard[8][8])
{

	return false;
}
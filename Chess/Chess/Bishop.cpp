#include <iostream>
#include <string>
#include "Piece.h"
#include "Board.h"
#include "Bishop.h"

Bishop::Bishop(Position source, char type, bool isWhite) : Piece(source, type, isWhite)
{

}
Bishop::~Bishop()
{

}
bool Bishop::isLegalMove(Position dest, Piece* chessBoard[8][8])
{
	int posY = this->getPos().getY();
	int posX = this->getPos().getX();
	int yDist = posY - dest.getY();
	int xDist = posX - dest.getX();

	if (yDist < 0) {
		yDist = yDist * -1;
	}

	if (xDist < 0) {
		xDist = xDist * -1;
	}

	if (yDist == xDist) {

		return true;
	}
	
	return false;
}

bool Bishop::isBlocked(Position dest, Piece* chessBoard[8][8])
{
	int posY = this->getPos().getY();
	int posX = this->getPos().getX();
	int i = 0;
	int j = 0;
	/*
	//right
	if (posX < dest.getX())
	{
		if (posY < dest.getY())
		{
			for (i = this->getPos().getX() + 1; i < dest.getX(); i++) {
				for (j = this->getPos().getY() + 1; j < dest.getY(); j++)
				{
					if (chessBoard[j][i] != nullptr) {
						return true;
					}
				}
			}
		}
		else if (posY > dest.getY())
		{
			for (i = this->getPos().getX() - 1; i < dest.getX(); i++) {
				for (j = this->getPos().getY() - 1; j > dest.getY(); j--)
				{
					if (chessBoard[j][i] != nullptr) {
						return true;
					}
				}

			}
		}
	}
	else if (posX > dest.getX())
	{
		if (posY < dest.getY())
		{
			for (i = this->getPos().getX() - 1; i > dest.getX(); i--) {
				for (j = this->getPos().getY() + 1; j < dest.getY(); j++)
				{
					if (chessBoard[j][i] != nullptr) {
						return true;
					}
				}
			}
		}
		else if (posY > dest.getY())
		{
			for (i = this->getPos().getX() - 1; i > dest.getX(); i--) {
				for (j = this->getPos().getY() - 1; j > dest.getY(); j--)
				{
					if (chessBoard[j][i] != nullptr) {
						return true;
					}
				}

			}
		}
	}
	*/
	
	return false;
}
#pragma once
#include <iostream>
#include <string>
#include "Piece.h"

class Piece;

class Board 
{
public:

	/*Board(Piece* pieces[8][8]);*/
	Board();
	~Board();

	//bool isTheStringGood(std::string str);
	std::string movePiece(std::string str);

	Position translator(std::string str);
	int getPlayer();
	int checkForCheck();//0- no check 1- white king 2- black;
	void changePosition(Position source, Position dest);
	void switchPlayer();
	void printBoard();
	
	//getters
	Piece** getPieces();

private:
	Position _blackKing;
	Position _whiteKing;
	int _player;
	Piece* pieces[8][8];
};



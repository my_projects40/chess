#pragma once
#include <iostream>
#include <string>
#include "Position.h"
#include "Board.h"

class Board;

class Piece
{
public:

	// Constructor
	Piece(const Position pos, char id, bool isWhite);

	//Piece();
	// 
	// Destructor
	~Piece();

	// Getters
	Position getPos();
	char getId();
	bool getisWhite();

	//Setters
	void setPos(Position pos);
	void setisWhite(bool isWhite);
	void setId(char id);


	//Methods
	virtual bool isLegalMove(Position dest, Piece* chessBoard[8][8]) = 0;
	virtual bool isBlocked(Position dest, Piece* chessBoard[8][8]) = 0;
	
private:

	Position _pos;
	char _id;
	bool _isWhite;
};